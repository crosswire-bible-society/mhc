#!/bin/bash
## First convert to usfm. Then to osis with u2o.py
#wget https://ccel.org/ccel/h/henry/mhc1.xml https://ccel.org/ccel/h/henry/mhc2.xml https://ccel.org/ccel/h/henry/mhc3.xml https://ccel.org/ccel/h/henry/mhc4.xml https://ccel.org/ccel/h/henry/mhc5.xml https://ccel.org/ccel/h/henry/mhc6.xml
rm -r temp/
mkdir temp
cp mhc*xml temp/
cd temp
rename 's/xml/txt/g' *xml
##Suppression retour chariot DOS
sed -i '1,134d' *.txt
sed -i 's/\r//' *.txt
sed -i 's/<a class="TOC".*$//g' *txt
sed -ri 's/^.*<\/td>//g' *txt
##Versets
sed -ri 's/<scripCom type="Commentary" passage="[A-Za-z1-9]* [0-9]*:[0-9]*-[0-9]*" id="[A-Za-z1-9]*\.[a-z]*-p[0-9]*\.[0-9]*" parsed="\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9]*" osisRef="Bible:[A-Za-z1-3]*\.[0-9]*\.([0-9]*)-[A-Za-z1-3]*\.[0-9]*\.([0-9]*)"\/>/\n\\v \1\-\2 /g' *.txt
sed -ri 's/<scripCom type="Commentary" passage="[A-Za-z1-9]* [0-9]*:[0-9]*-[0-9]*" id="[A-Za-z1-9]*\.[a-z]*-p[0-9]*\.[0-9]*_[0-9]*" parsed="\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9]*" osisRef="Bible:[A-Za-z1-3]*\.[0-9]*\.([0-9]*)-[A-Za-z1-3]*\.[0-9]*\.([0-9]*)"\/>/\n\\v \1\-\2 /g' *.txt
sed -ri 's/<scripCom type="Commentary" passage="[A-Za-z1-9]* [0-9]*:[0-9]*" id="[A-Za-z1-9]*\.[a-z]*-p[0-9]*\.[0-9]*" parsed="\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9]*" osisRef="Bible:[A-Za-z1-3]*\.[0-9]*\.([0-9]*)"\/>/\n\\v \1 /g' *.txt
##Verset dans mhc4.xml
sed -ri 's/<scripCom id="[A-Za-z1-3]*\.[a-z]*-p[0-9]*\.[0-9]*" osisRef="Bible:[A-Za-z1-3]*\.[0-9]*\.([0-9]*)-[A-Za-z1-3]*\.[0-9]*\.([0-9]*)" parsed="\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9]*" passage="[A-Za-z1-9]* [0-9]*:[0-9]*-[0-9]*" type="Commentary"><\/scripCom>/\n\\v \1-\2 /g' *.txt
sed -ri 's/<scripCom id="[A-Za-z1-3]*\.[a-z]*-p[0-9]*\.[0-9]*" osisRef="Bible:[A-Za-z1-3]*\.[0-9]*\.([0-9]*)" parsed="\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9]*" passage="[A-Za-z1-9]* [0-9]*:[0-9]*" type="Commentary"><\/scripCom>/\n\\v \1 /g' *.txt

sed -ri 's/<\/div>|<\/div2>//g' *.txt
sed -ri 's/<\/chapter>//g' *.txt
sed -ri 's/<div class="Commentary" id="Bible:[A-Za-z1-3]*\.[0-9]*\.[0-9]*-[A-Za-z1-3]*\.[0-9]*\.[0-9]*">//g' *.txt

#Chapter

sed -ri 's/<h3 id="[A-Za-z1-3]*\.[a-z]*-p[0-9]*\.[0-9]*">(CHAP\.|PSALM) ([VICLX]*)\.<\/h3>/\\c \2\n\\p\n/g' *.txt
##Entrée dic
sed -ri 's/&#8217;/’/g' *.txt

sed -ri s'/&#8211;([0-9]\.)/\1/g' *.txt

sed -ri 's/<p class="index[0-9]*" id="[a-z]-p[0-9]*">/<lb\/>/g' *.txt
sed -ri 's/<\/p>//g' *.txt


##Crossref
sed -ri 's/passage="[A-Z1-3a-z]* [0-9]*:[0-9]*" id="[a-z]*-p[0-9]*\.[0-9]*" parsed="\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9]*" osisRef="Bible://g' *.txt
sed -ri 's/passage="[A-Z1-3a-z]* [0-9]*:[0-9]*" id="[A-Z1-3a-z]*\.[a-z]*-p[0-9]*\.[0-9]*" parsed="\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9]*" osisRef="Bible://g' *.txt

sed -ri 's/passage="[A-Z1-3a-z]* [0-9]*:[0-9]*[-,][0-9]*" id="[a-z]*-p[0-9]*\.[0-9]*" parsed="\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9]*" osisRef="Bible://g' *.txt
sed -ri 's/passage="[A-Z1-3a-z]* [0-9]*:[0-9]*[-,][0-9]*" id="[A-Z1-3a-z]*\.[a-z]*-p[0-9]*\.[0-9]*" parsed="\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9]*" osisRef="Bible://g' *.txt

sed -ri 's/<scripRef id="[A-Z1-3a-z]*\.[a-z]*-p[0-9]*\.[0-9]*" (osisRef="Bible:[A-Z1-3a-z]*\.[0-9]*\.[0-9]*") parsed="\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9]*" passage="[A-Z1-3a-z]* [0-9]*:[0-9]*/@@reference \1/g' *.txt
sed -ri 's/<scripRef id="[A-Z1-3a-z]\.[a-z]*-p[0-9]*\.[0-9]*" (osisRef="Bible:[A-Z1-3a-z]*\.[0-9]*\.[0-9]*-[A-Z1-3a-z]*\.[0-9]*\.[0-9]*") parsed="\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9]*" passage="[A-Z1-3a-z]* [0-9]*:[0-9]*-[0-9]*/@@reference \1/g' *.txt
sed -ri 's/passage=".*osisRef="Bible://g' *.txt #C'est lui qui supprime les multiples référecences
sed -ri 's/<scripRef id="[a-z]*-p[0-9]*\.[0-9]*" (osisRef="Bible:[A-Z1-3a-z]*\.[0-9]*\.[0-9]*-[A-Z1-3a-z]*\.[0-9]*\.[0-9]*") parsed="\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9]*" passage="[A-Z1-3a-z]* [0-9]*:[0-9]*-[0-9]*/@@reference \1/g' *.txt
sed -ri 's/passage=".*Bible://g' *.txt
sed -ri 's/<scripRef id="[a-z]*-p[0-9]*\.[0-9]*" (osisRef="Bible:[A-Z1-3a-z]*\.[0-9]*\.[0-9]*") parsed="\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9]*" passage="[A-Z1-3a-z]* [0-9]*:[0-9]*/@@reference \1/g' *.txt
sed -ri 's/<scripRef id="[a-z]*-p[0-9]*\.[0-9]*" (osisRef=")(Bible:[A-Z1-3a-z]*\.[0-9]*\.[0-9 ]*)*" parsed="(\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9;]*)*" passage="[A-Z1-3a-z]* [0-9]*:[0-9,]*/@@reference \1/g' *.txt

## Correction d'une fausse référence
sed -ri 's/Heb\.1\.15">Heb. i./Heb\.13\.15">Heb\. xiii\./g' *.txt
###Cette ligne remplace le nom du livre par son id et le chap 1.
sed -ri 's/<h2 id="i-p1\.2">(Genesis to Deuteronomy)<\/h2>/\\id GEN\n\\toc1 Genesis\n\\toc2\n\\toc3 Ge\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Ex\.i-p0\.1">(Exodus)<\/h2>/\\id EXO\n\\toc1 \1\n\\toc2\n\\toc3 Exod\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Lev\.i-p0\.1">(Leviticus)<\/h2>/\\id LEV\n\\toc1 \1\n\\toc2\n\\toc3 Lev\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Num\.i-p0\.1">(Numbers)<\/h2>/\\id NUM\n\\toc1 \1\n\\toc2\n\\toc3 Num\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Deu\.i-p0\.1">(Deuteronomy)<\/h2>/\\id DEU\n\\toc1 \1\n\\toc2\n\\toc3 Deut\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="i-p1\.2">(Joshua to Esther)<\/h2>/\\id JOS\n\\toc1 Joshua\n\\toc2\n\\toc3 Josh\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Jud\.i-p0\.1">(Judges)<\/h2>/\\id JDG\n\\toc1 \1\n\\toc2\n\\toc3 Judg\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Ru\.i-p0\.1">(Ruth)<\/h2>/\\id RUT\n\\toc1 \1\n\\toc2\n\\toc3 Ruth\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="iSam\.i-p0\.1">(First Samuel)<\/h2>/\\id 1SA\n\\toc1 \1\n\\toc2\n\\toc3 1 Sam\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="iiSam\.i-p0\.1">(Second Samuel)<\/h2>/\\id 2SA\n\\toc1 \1\n\\toc2\n\\toc3 2 Sam\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="iKi\.i-p0\.1">(First Kings)<\/h2>/\\id 1KI\n\\toc1 \1\n\\toc2\n\\toc3 1 Kings\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="iiKi\.i-p0\.1">(Second Kings)<\/h2>/\\id 2KI\n\\toc1 \1\n\\toc2\n\\toc3 2 Kings\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="iCh\.i-p0\.1">(First Chronicles)<\/h2>/\\id 1CH\n\\toc1 \1\n\\toc2\n\\toc3 1 Chron\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="iiCh\.i-p0\.1">(Second Chronicles)<\/h2>/\\id 2CH\n\\toc1 \1\n\\toc2\n\\toc3 2 Chron\n\\c 1\n\\ms \1\n\\p\n\\v 1/g' *.txt
sed -ri 's/<h2 id="Ez\.i-p0\.1">(Ezra)<\/h2>/\\id EZR\n\\toc1 \1\n\\toc2\n\\toc3 Ezra\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Neh\.i-p0\.1">(Nehemiah)<\/h2>/\\id NEH\n\\toc1 \1\n\\toc2\n\\toc3 Neh\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Esth\.i-p0\.1">(Esther)<\/h2>/\\id EST\n\\toc1 \1\n\\toc2\n\\toc3 Esth\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h4 id="iii-p0\.3">(Job to Song of Solomon)<\/h4>/\\id JOB\n\\toc1 Job\n\\toc2\n\\toc3 Job\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Ps\.i-p0\.1">(Psalms)<\/h2>/\\id PSA\n\\toc1 \1\n\\toc2\n\\toc3 Ps\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Prov\.i-p0\.1">(Proverbs)<\/h2>/\\id PRO\n\\toc1 \1\n\\toc2\n\\toc3 Prov\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Ec\.i-p0\.1">(Ecclesiastes)<\/h2>/\\id ECC\n\\toc1 \1\n\\toc2\n\\toc3 Eccl\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Song\.i-p0\.1">(Song of Solomon)<\/h2>/\\id SNG\n\\toc1 \1\n\\toc2\n\\toc3 Cant\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h4 id="iv-p0\.3">(Isaiah to Malachi)<\/h4>/\\id ISA\n\\toc1 Isaiah\n\\toc2\n\\toc3 Isa\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Jer\.i-p0\.1">(Jeremiah)<\/h2>/\\id JER\n\\toc1 \1\n\\toc2\n\\toc3 Jer\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Lam\.i-p0\.1">(Lamentations)<\/h2>/\\id LAM\n\\toc1 \1\n\\toc2\n\\toc3 Lam\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Ez\.i-p0\.1">(Ezekiel)<\/h2>/\\id EZK\n\\toc1 \1\n\\toc2\n\\toc3 Ezek\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Dan\.i-p0\.1">(Daniel)<\/h2>/\\id DAN\n\\toc1 \1\n\\toc2\n\\toc3 Dan\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Hos\.i-p0\.1">(Hosea)<\/h2>/\\id HOS\n\\toc1 \1\n\\toc2\n\\toc3 Hos\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Joel\.i-p0\.1">(Joel)<\/h2>/\\id JOL\n\\toc1 \1\n\\toc2\n\\toc3 Joel\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Amos\.i-p0\.1">(Amos)<\/h2>/\\id AMO\n\\toc1 \1\n\\toc2\n\\toc3 Amos\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Obad\.i-p0\.1">(Obadiah)<\/h2>/\\id OBA\n\\toc1 \1\n\\toc2\n\\toc3 Obad\n\\mt \1\n\\p\n\\c 1\n\\p/g' *.txt
sed -ri 's/<h2 id="Jonah\.i-p0\.1">(Jonah)<\/h2>/\\id JON\n\\toc1 \1\n\\toc2\n\\toc3 Jon\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Mic\.i-p0\.1">(Micah)<\/h2>/\\id MIC\n\\toc1 \1\n\\toc2\n\\toc3 Mic\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Nah\.i-p0\.1">(Nahum)<\/h2>/\\id NAM\n\\toc1 \1\n\\toc2\n\\toc3 Nah\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Hab\.i-p0\.1">(Habakkuk)<\/h2>/\\id HAB\n\\toc1 \1\n\\toc2\n\\toc3 Hab\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Zeph\.i-p0\.1">(Zephaniah)<\/h2>/\\id ZEP\n\\toc1 \1\n\\toc2\n\\toc3 Zeph\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Hag\.i-p0\.1">(Haggai)<\/h2>/\\id HAG\n\\toc1 \1\n\\toc2\n\\toc3 Hag\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Zech\.i-p0\.1">(Zechariah)<\/h2>/\\id ZEC\n\\toc1 \1\n\\toc2\n\\toc3 Zech\n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Mal\.i-p0\.1">(Malachi)<\/h2>/\\id MAL\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h4 id="ii-p0\.3">(Matthew to John)<\/h4>/\\id MAT\n\\toc1 Matthew\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Mark\.i-p0\.1">(Mark)<\/h2>/\\id MRK\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Luke\.i-p0\.1">(Luke)<\/h2>/\\id LUK\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="John\.i-p0\.1">(John)<\/h2>/\\id JHN\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="i-p1\.2">(Acts to Revelation)<\/h2>/\\id ACT\n\\toc1 Acts\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Rom\.i-p0\.1">(Romans)<\/h2>/\\id ROM\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="iCor\.i-p0\.1">(<h2 id="iCor.i-p0.2">First Corinthians)<\/h2>/\\id 1CO\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="iiCor\.i-p0\.1"><h2 id="iiCor.i-p0.2">(Second Corinthians)<\/h2>/\\id 2CO\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="Gal\.i-p0\.1"><h2 id="Gal.i-p0.2">(Galatians)<\/h2>/\\id GAL\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="Eph\.i-p0\.1"><h2 id="Eph.i-p0.2">(Ephesians)<\/h2>/\\id EPH\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="Phi\.i-p0\.1"><h2 id="Phi.i-p0.2">(Philippians)<\/h2>/\\id PHP\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="Col\.i-p0\.1"><h2 id="Col.i-p0.2">(Colossians)<\/h2>/\\id COL\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="iTh\.i-p0\.1"><h2 id="iTh.i-p0.2">(First Thessalonians)<\/h2>/\\id 1TH\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="iiTh\.i-p0\.1"><h2 id="iiTh.i-p0.2">(Second Thessalonians)<\/h2>/\\id 2TH\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="iTim\.i-p0\.1"><h2 id="iTim.i-p0.2">(First Timothy)<\/h2>/\\id 1TI\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="iiTim\.i-p0\.1"><h2 id="iiTim.i-p0.2">(Second Timothy)<\/h2>/\\id 2TI\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Tit\.i-p0\.1">(Titus)<\/h2>/\\id TIT\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="Phm\.i-p0\.1">(Philemon)<\/h2>/\\id PHM\n\\toc1 \1\n\\toc2\n\\toc3 \n\\mt \1\n\\p\n\\c 1\n\\p/g' *.txt

sed -ri 's/<div class="Center" id="Heb\.i-p0\.1"><h2 id="Heb\.i-p0\.2">(Hebrews)<\/h2>/\\id HEB\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="Jam\.i-p0\.1"><h2 id="Jam\.i-p0\.2">J(ames)<\/h2>/\\id JAS\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="iPet\.i-p0\.1"><h2 id="iPet\.i-p0\.2">(First Peter)<\/h2>/\\id 1PE\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="iiPet\.i-p0\.1"><h2 id="iiPet\.i-p0\.2">(Second Peter)<\/h2>/\\id 2PE\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="iJo\.i-p0\.1"><h2 id="iJo\.i-p0\.2">(First John)<\/h2>/\\id 1JN\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt
sed -ri 's/<h2 id="iiJo\.i-p0\.1">(Second John)<\/h2>/\\id 2JN\n\\toc1 \1\n\\toc2\n\\toc3 \n\\mt \1\n\\p\n\\c 1\n\\p/g' *.txt
sed -ri 's/<h2 id="iiiJo\.i-p0\.1">(Third John)<\/h2>/\\id 3JN\n\\toc1 \1\n\\toc2\n\\toc3 \n\\mt \1\n\\p\n\\c 1\n\\p/g' *.txt
sed -ri 's/<h2 id="Ju\.i-p0\.1">(Jude)<\/h2>/\\id JUD\n\\toc1 \1\n\\toc2\n\\toc3 \n\\mt \1\n\\p\n\\c 1\n\\p/g' *.txt
sed -ri 's/<div class="Center" id="Rev\.i-p0\.1"><h2 id="Rev.i-p0.2">(Revelation)<\/h2>/\\id REV\n\\toc1 \1\n\\toc2\n\\toc3 \n\\c 1\n\\ms \1\n\\p/g' *.txt



##Paragraphe
sed -ri 's/<p class="indent" id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*\.[0-9]*">/\\p /g' *txt
sed -ri 's/<p class="indent" id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*">/\\p /g' *txt
sed -ri 's/<p class="indent" id="[a-z]*-p[0-9]*">/\\p /g' *txt
sed -ri 's/(<p class="passage" id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*">|<p class="passage" id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*" shownumber="no">)([0-9]*)/\\p \\sup \2\\sup\*/g' *txt
sed -ri 's/<p class="passage" id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*" shownumber="no">/\\p /g' *txt
##Paragraphe intro
sed -ri 's/<p class="intro" id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*">/\\ip /g' *txt
sed -ri 's/<p class="intro" id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*" shownumber="no">/\\ip /g' *txt
sed -ri 's/<p class="indent" id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*">/\\p /g' *txt
sed -ri 's/<p class="indent" id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*" shownumber="no">/\\p /g' *txt
sed -ri 's/<p class="indent" id="[a-z]*-p[0-9]*" shownumber="no">/\\p /g' *txt

##Paragraphe en citation
sed -ri 's/<l class="t[0-9]" id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*\.[0-9]*">(.*)<\/l>/\\q \1/g' *txt
sed -ri 's/<\/l>//g' *txt
sed -ri 's/<l class="t[0-9]" id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*\.[0-9]*">(.*)$/\\q \1/g' *txt
sed -ri 's/<(l|li) class="small" id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*\.[0-9]*">/\\q /g' *txt
sed -ri 's/<l id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*\.[0-9]*">/\\q /g' *txt
sed -ri 's/<\/li>//g' *txt
##Titres
sed -ri 's/<h4 id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*">|<h4 id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*\.[0-9]*">|<h4 id="[a-z]*-p[0-9]*\.[0-9]*">/\\s3 /g' *txt
#|<h4 id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*\.[0-9]*">

sed -ri 's/<h3 id="[a-z]*-p[0-9]*\.[0-9]*">P R E F A C E\.<\/h3>/\\s2 PREFACE/g' *txt
sed -ri 's/<h3 id="[A-Za-z1-4]*\.[a-z]*-p[0-9]*\.[0-9]*">.*$//g' *txt
sed -i "s/’/'/g" *.txt

##Prép nom des livres
sed -ri '/^ *$/d' *txt
sed -ri ':a;N;$!ba;s/^ *<div1 .*">\n <h2.*>([A-Z1-2][A-Za-z]*)<\/h2>/\1/g' *txt
##Suppression chap I 
sed -ri ':a;N;$!ba;s/\\c I\n\\p//g' *txt


sed -ri ':a;N;$!ba;s/\n([a-zA-Z1-9]|\\it )/ \1/g' *txt


##suppression truc inutile
sed -ri 's/<\/glossary>|<\/div2>|<div2.*>|<h2 id="[a-z]-p[0-9.]*">[A-Z]*<\/h2>|<glossary id="[a-z]-p[0-9.]*">|<h2 id="[A-Z1-2][A-Za-z]*\.[a-z]*-p[0-9]*\.[0-9]*">.*$|<pb n="[0-9]*" id="[A-Z1-2][A-Za-z]*\.[a-z]*-Page_[0-9]*"\/>|<\/div1>|<div1 .*$|<\/div>|<hr\/>|<scripCom type="Commentary".*$|<div class="Center" id=".*$|<p class="bref">.*$|<p id=".*$|<h5 id=".*$|\\s3 W I T H.*$|<p class="bbook">.*$|<hr>.*<\/hr>|<hr.*\/>$|<tr.*$|<td.*>|<\/tr>|<\/table>|<\/h4>|<table.*>$|<div class="Commentary".*$|<scripCom.*$|<\/verse>|<verse.*>$|h[12345].*$//g' *.txt

sed -ri 's/ *(\\[ivc])/\1/g' *.txt
../Roman_to_Decimal.sh
sed -ri 's/<p class="Center" id=".*">(Completed by)/\\s4 \1/g' *.txt
sed -ri 's/&#8212;/—/g' *.txt
##Sup
sed -ri 's/( &#160; |  )([0-9]*) / \\sup \2\\sup\* /g' *.txt
sed -ri 's/&#160;/ /g;s/\&amp;/\&/g' *.txt

##Italic et petites capitales
sed -ri 's/<b><i>/\\bdit /g;s/<\/i><\/b>/\\bdit\*/g' *txt
sed -ri 's/<i>/\\it /g;s/<\/i>/\\it\*/g' *txt
sed -ri 's/\\it (ch\.|v\.)\\it\*/\1/g' *txt
sed -ri 's/<span class="smallcaps" id="[A-Z1-3a-z]*\.[a-z]*-p[0-9]*\.[0-9]*">|<span class="smallcaps" id="[a-z]*-p[0-9]*\.[0-9]*">/\\sc /g;s/<\/span>/\\sc\* /g' *txt
##Notes
sed -ri 's/<note n="[0-9]*" id="[pi.0-9-]*">|<note anchored="yes" id="[pvxi.0-9-]*" n="[0-9]*" place="foot">/\\f \+ \\ft /g;s/<\/note>/\\f\*/g' *txt

##Ref et correction de ce reste de défecteux
sed -ri 's/<scripRef /@@reference osisRef="/g' *.txt
sed -ri 's/<\/scripRef>/@@\/referenceµµ/g' *.txt

sed -ri 's/">/"µµ/g' *.txt
sed -ri 's/""/"/g' *.txt
sed -ri 's/^( +)<(p class=|!--|insertIndex).+(µµ|\/>|>)$//g' *.txt
sed -ri 's/<\/ThML\.body>|<p class="Center" id="iiJo.i-p1"µµ //g' *txt
sed -ri 's/^<(p class=|!--|div class|l class=|ol).*(µµ|\/>|>|[a-z])$//g' *.txt
sed -ri 's/^<(\/ThML|hr .*|pb .*|\/ol)>$//g;s/^( <|<| T H E S S A L O N I A N S.<\/|)$//g' *.txt
sed -ri 's/<attr id.*µµ|<\/attr>|<l id=.*\/>|<!-- .*//g;s/<span class="sic"µµthe\\sc\*/the/g' *txt
sed -ri 's/<!-- <a href="MHC[0-9]*.HTM" id="/@@reference osisRef="/g' *txt

sed -ri 's/<!-- <\/a> -->/@@\/referenceµµ/g;s/-->/→/g' *.txt

sed -i 's/  / /g' *.txt
sed -i 's/(^ *| <)$//g' *.txt
sed -i '/^$/d' *txt
sed -ri 's/ Bible:([0-9A-Z])/ \1/g;s/(osisRef=")Bible:/\1/g' *txt
sed -ri 's/([0-9]):([0-9])/\1\.\2/g' *.txt
sed -ri 's/([.,])([0-9]*),([0-9A-Z])/\1\2 \3/g;s/([0-9]),([A-Z])/\1 \2/g;s/(\.| )([0-9]*),([0-9]*\.)/\1\2 \3/g' *.txt
sed -ri 's/(\.[0-9]*),([0-9A-Z])/\1 \2/g' *.txt
sed -ri 's/\\p (\\sc This\\sc\* is the shortest of all)/\1/g' *.txt
sed -ri 's/(osisRef=")passage="([A-Z1-3a-z]*) /\1\2\./g;s/(osisRef=")(Php\.)([0-9]*\.)([0-9]*),([0-9]*)\.([0-9]*)"/\1\2\3\4 \5\.\6/g' *.txt

sed -ri 's/ parsed="\|[A-Z1-3a-z]*\|[A-Z0-9a-z|;]*" passage="[A-Z0-9a-z|,: -.]*[0-9.]"//g' *txt
sed -ri 's/(osisRef=")id="[a-z]*-p[0-9]*\.[0-9]*" osisRef="/\1/g' *txt
sed -ri 's/(osisRef=")(id="[a-zA-Z]*\.[a-z]*-p[0-9]*\.[0-9]*" |osisRef="|id="[a-z]*-p[0-9]*\.[0-9]*" passage=")/\1/g' *txt
sed -ri 's/ parsed="\|[A-Z1-3a-z]*\|[0-9]*\|[0-9]*\|[0-9]*\|[0-9]*" passage="[A-Z1-3a-z]* [0-9]*:[0-9,]*"//g' *txt
sed -ri 's/osisRef="id="[a-z]*-p[0-9]*\.[0-9]* (osisRef=")/\1/g;s/osisRef="[a-zA-Z]*\.[0-9]*\.[0-9]*-[a-zA-Z]*\.[0-9]*\.[0-9]*" parsed="\|[A-Z1-3a-z]*\|[A-Z0-9a-z|;]*" /osisRef="/g' *txt
sed -ri 's/" parsed="\|2Macc\|7\|0\|0\|0" passage="2 Mac\. vii| parsed="\|1Macc\|9\|28\|0\|0;\|1Macc\|10\|1\|0\|0" passage="1 Mac\. ix\. 28; x\. 1"|1Kgs\.20\.35" parsed="\|1Kgs\|20\|35\|0\|0" |Jer\.7\.3 Jer\.7\.5" parsed="\|Jer\|7\|3\|0\|0;\|Jer\|7\|5\|0\|0" | id="Ps\.xcviii-p12\.2"//g;s/Jer\.48\.1-Jer\.48\.6 Jer\.48\.8" parsed="\|Jer\|48\|1\|48\|6;\|Jer\|48\|8\|0\|0" Jer\.48\.34/Jer\.48\.21-Jer\.48\.25 Jer\.48\.34/g;s/10,14/10 14/g' *txt
sed -ri 's/(osisRef=")osisRef="/\1/g;;s/osisRef="passage="/osisRef="/g' *txt
sed -ri 's/@@reference osisRef="µµ(Deut\. xviii\. 15, 18)/@@reference osisRef="Deut\.18\.15 Deut\.18\.18"µµ\1/g;s/@@reference osisRef="µµ(Ps. li. 10, 12)/@@reference osisRef="Psa\.51\.105 Psa\.51\.12"µµ\1/g' *txt
sed -ri 's/" id="[A-Z1-3a-z]*\.[a-z]*-p[0-9]*\.[0-9]*"µµ/"µµ/g' *txt

awk 'BEGIN{file="./"(FILENAME)"0.txt"}/\\id/{file="./"(FILENAME)(++i)".txt"}{print > file}' *.txt
sed -ri ':a;N;$!ba;s/\\p\n J O H N<\/\n\\s3 THE DIVINE\./\\s3 AN EXPOSITION, WITH PRACTICAL OBSERVATIONS, OF THE REVELATION OF ST. JOHN THE DIVINE/g' *.txt
sed -ri 's/\&#230;/æ/g' *.txt
rm mhc[1-6].txt
rm 0.txt

u2o.py -e utf-8 -l en -o mhc.osis.xml -v -d MHC -x *.txt
sed -ri 's/@@/</g' mhc.osis.xml
sed -ri 's/µµ/>/g' mhc.osis.xml

#sed -ri 's/<reference osisRef=">(Deut\. xviii\. 15, 18)/<reference osisRef="Deut\.18\.15 Deut\.18\.18">\1/g;s/<reference osisRef=">(Ps. li. 10, 12)/<reference osisRef="Psa\.51\.105 Psa\.51\.12">\1/g' mhc.osis.xml
#sed -ri 's/" id="[A-Z1-3a-z]*\.[a-z]*-p[0-9]*\.[0-9]*"µµ/"µµ/g' mhc.osis.xml

sed -ri 's/(<reference osisRef=")id="Hos\.i-p2\.2("><hi type="italic">Ecclus\.<\/hi> xlix\. 10<\/reference>)/\1Sir\.49\.10\2/g;s/(<reference osisRef=")id="Heb\.xii-p90\.2(">2 Macc\. ch\. vii\.<\/reference>)/\11Mac\.11\.59\2/g;s/(<reference osisRef=")id="Esth\.v-p14\.2(">ch\. xiii\. and xiv\.<\/reference>)/\1Esth\.4\.17\2/g;s/(<reference osisRef=")id="Dan\.xii-p20\.7(">v\. 59<\/reference>)/\11Mac\.11\.59\2/g;s/<reference osisRef="id="Ez\.vii-p10\.3">(Book I\. ch\. iii\. and iv\.)<\/reference>/<hi type="underline">\1<\/hi>/g' mhc.osis.xml

#xmllint --noout --schema ~/.bin/osisCore.2.1.1.xsd mhc.osis.xml
mkdir ~/.sword/modules/comments/zcom/mhc
osis2mod ~/.sword/modules/comments/zcom/mhc/ mhc.osis.xml -s 4 -z >out.log
for FILE in *.txt ; do
rename 's/txt/usfm/g' "$FILE"
done

